﻿using System;
using UnityEngine;
using System.Reflection;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

public struct MyMatrix4x4 : IEquatable<MyMatrix4x4>
{
    public float m00;
    public float m33;
    public float m23;
    public float m13;
    public float m03;
    public float m32;
    public float m22;
    public float m02;
    public float m12;
    public float m21;
    public float m11;
    public float m01;
    public float m30;
    public float m20;
    public float m10;
    public float m31;

    public MyMatrix4x4(Vector4 column0, Vector4 column1, Vector4 column2, Vector4 column3)
    {
        m00 = column0.x; m01 = column1.x; m02 = column2.x; m03 = column3.x;
        m10 = column0.y; m11 = column1.y; m12 = column2.y; m13 = column3.y;
        m20 = column0.z; m21 = column1.z; m22 = column2.z; m23 = column3.z;
        m30 = column0.w; m31 = column1.w; m32 = column2.w; m33 = column3.w;
    }

    public float this[int index]
    {
        get
        {
            switch (index)
            {
                case 0: return m00;
                case 1: return m10;
                case 2: return m20;
                case 3: return m30;
                case 4: return m01;
                case 5: return m11;
                case 6: return m21;
                case 7: return m31;
                case 8: return m02;
                case 9: return m12;
                case 10: return m22;
                case 11: return m32;
                case 12: return m03;
                case 13: return m13;
                case 14: return m23;
                case 15: return m33;
                default:
                    throw new IndexOutOfRangeException("Invalid matrix index!");
            }
        }

        set
        {
            switch (index)
            {
                case 0: m00 = value; break;
                case 1: m10 = value; break;
                case 2: m20 = value; break;
                case 3: m30 = value; break;
                case 4: m01 = value; break;
                case 5: m11 = value; break;
                case 6: m21 = value; break;
                case 7: m31 = value; break;
                case 8: m02 = value; break;
                case 9: m12 = value; break;
                case 10: m22 = value; break;
                case 11: m32 = value; break;
                case 12: m03 = value; break;
                case 13: m13 = value; break;
                case 14: m23 = value; break;
                case 15: m33 = value; break;

                default:
                    throw new IndexOutOfRangeException("Invalid matrix index!");
            }
        }
    }

    public float this[int row, int column]
    {
        get
        {
            return this[row + column * 4];
        }

        set
        {
            this[row + column * 4] = value;
        }
    }

    public static MyMatrix4x4 zero 
    { 
        get
        { 
            return new MyMatrix4x4(new Vector4(0, 0, 0, 0), new Vector4(0, 0, 0, 0), new Vector4(0, 0, 0, 0), new Vector4(0, 0, 0, 0)); 
        } 
    }

    public static MyMatrix4x4 identity 
    { 
        get 
        { 
            return new MyMatrix4x4(new Vector4(1, 0, 0, 0), new Vector4(0, 1, 0, 0), new Vector4(0, 0, 1, 0), new Vector4(0, 0, 0, 1));
        } 
    }

    public MyMatrix4x4 transpose { get { return Transpose(this); } }

    public Quaternion rotation 
    { 
        get 
        {
            Vector3 s = lossyScale;

            // Normalize Scale from Matrix4x4
            float _m00 = m00 / s.x;
            float _m01 = m01 / s.y;
            float _m02 = m02 / s.z;
            float _m10 = m10 / s.x;
            float _m11 = m11 / s.y;
            float _m12 = m12 / s.z;
            float _m20 = m20 / s.x;
            float _m21 = m21 / s.y;
            float _m22 = m22 / s.z;

            Quaternion q = new Quaternion();

            //Max(0, ) para evitar un error de redondeo
            q.w = Mathf.Sqrt(Mathf.Max(0, 1 + _m00 + _m11 + _m22)) / 2;
            q.x = Mathf.Sqrt(Mathf.Max(0, 1 + _m00 - _m11 - _m22)) / 2;
            q.y = Mathf.Sqrt(Mathf.Max(0, 1 - _m00 + _m11 - _m22)) / 2;
            q.z = Mathf.Sqrt(Mathf.Max(0, 1 - _m00 - _m11 + _m22)) / 2;
            q.x *= Mathf.Sign(q.x * (_m21 - _m12));
            q.y *= Mathf.Sign(q.y * (_m02 - _m20));
            q.z *= Mathf.Sign(q.z * (_m10 - _m01));

            q.Normalize();

            return q;
        } 
    }

    public Vector3 lossyScale 
    {
        get
        {
            return new Vector3 (GetColumn(0).magnitude, GetColumn(1).magnitude, GetColumn(2).magnitude);
        }
    }

    public MyMatrix4x4 inverse { get { return Inverse(this); } }

    public static MyMatrix4x4 Inverse(MyMatrix4x4 m)
    { 
        //encontyramos la determinante de la matriz haciendo producto cruz de la matriz
        float det = m.m00 * m.m11 * m.m22 * m.m33
                  + m.m10 * m.m21 * m.m32 * m.m03
                  + m.m20 * m.m31 * m.m02 * m.m13
                  + m.m30 * m.m01 * m.m12 * m.m23
                  - m.m03 * m.m12 * m.m21 * m.m30
                  - m.m13 * m.m22 * m.m31 * m.m00
                  - m.m23 * m.m32 * m.m01 * m.m10
                  - m.m33 * m.m02 * m.m11 * m.m20;

        //realizamos la adjunta de la matriz
        MyMatrix4x4 detsDeAdjunta = identity;

        detsDeAdjunta.m00 = m.m11 * m.m22 * m.m33 + m.m12 * m.m23 * m.m31 + m.m13 * m.m21 * m.m32
                          - m.m13 * m.m22 * m.m31 - m.m12 * m.m21 * m.m33 - m.m11 * m.m23 * m.m32;

        detsDeAdjunta.m10 = -(m.m01 * m.m22 * m.m33 + m.m02 * m.m23 * m.m31 + m.m03 * m.m21 * m.m32
                          - m.m03 * m.m22 * m.m31 - m.m02 * m.m21 * m.m33 - m.m01 * m.m23 * m.m32);

        detsDeAdjunta.m20 = m.m01 * m.m12 * m.m33 + m.m02 * m.m13 * m.m31 + m.m03 * m.m11 * m.m32
                          - m.m03 * m.m12 * m.m31 - m.m02 * m.m11 * m.m33 - m.m01 * m.m13 * m.m32;

        detsDeAdjunta.m30 = -(m.m01 * m.m12 * m.m23 + m.m02 * m.m13 * m.m21 + m.m03 * m.m11 * m.m22
                          - m.m03 * m.m12 * m.m21 - m.m02 * m.m11 * m.m23 - m.m01 * m.m13 * m.m22);

        detsDeAdjunta.m01 = -(m.m10 * m.m22 * m.m33 + m.m12 * m.m23 * m.m30 + m.m13 * m.m20 * m.m32
                          - m.m13 * m.m22 * m.m30 - m.m12 * m.m20 * m.m33 - m.m10 * m.m23 * m.m32);

        detsDeAdjunta.m11 = m.m00 * m.m22 * m.m33 + m.m02 * m.m23 * m.m30 + m.m03 * m.m20 * m.m32
                          - m.m03 * m.m22 * m.m30 - m.m02 * m.m20 * m.m33 - m.m00 * m.m23 * m.m32;

        detsDeAdjunta.m21 = -(m.m00 * m.m12 * m.m33 + m.m02 * m.m13 * m.m30 + m.m03 * m.m10 * m.m32
                          - m.m03 * m.m12 * m.m30 - m.m02 * m.m10 * m.m33 - m.m00 * m.m13 * m.m32);

        detsDeAdjunta.m31 = m.m00 * m.m12 * m.m23 + m.m02 * m.m13 * m.m20 + m.m03 * m.m10 * m.m22
                          - m.m03 * m.m12 * m.m20 - m.m02 * m.m10 * m.m23 - m.m00 * m.m13 * m.m22;

        detsDeAdjunta.m02 = m.m10 * m.m21 * m.m33 + m.m11 * m.m23 * m.m30 + m.m13 * m.m20 * m.m31
                          - m.m13 * m.m21 * m.m30 - m.m11 * m.m20 * m.m33 - m.m10 * m.m23 * m.m31;

        detsDeAdjunta.m12 = -(m.m00 * m.m21 * m.m33 + m.m01 * m.m23 * m.m30 + m.m03 * m.m20 * m.m31
                          - m.m03 * m.m21 * m.m30 - m.m01 * m.m20 * m.m33 - m.m00 * m.m23 * m.m31);

        detsDeAdjunta.m22 = m.m00 * m.m11 * m.m33 + m.m01 * m.m13 * m.m30 + m.m03 * m.m10 * m.m31
                          - m.m03 * m.m11 * m.m30 - m.m01 * m.m10 * m.m33 - m.m00 * m.m13 * m.m31;

        detsDeAdjunta.m32 = -(m.m00 * m.m11 * m.m23 + m.m01 * m.m13 * m.m20 + m.m03 * m.m10 * m.m21
                          - m.m03 * m.m11 * m.m20 - m.m01 * m.m10 * m.m23 - m.m00 * m.m13 * m.m21);

        detsDeAdjunta.m03 = -(m.m10 * m.m21 * m.m32 + m.m11 * m.m22 * m.m30 + m.m12 * m.m20 * m.m31
                          - m.m12 * m.m21 * m.m30 - m.m11 * m.m20 * m.m32 - m.m10 * m.m22 * m.m31);

        detsDeAdjunta.m13 = m.m00 * m.m21 * m.m32 + m.m01 * m.m22 * m.m30 + m.m02 * m.m20 * m.m31
                          - m.m02 * m.m21 * m.m30 - m.m01 * m.m20 * m.m32 - m.m00 * m.m22 * m.m31;

        detsDeAdjunta.m23 = -(m.m00 * m.m11 * m.m32 + m.m01 * m.m12 * m.m30 + m.m02 * m.m10 * m.m31
                          - m.m02 * m.m11 * m.m30 - m.m01 * m.m10 * m.m32 - m.m00 * m.m12 * m.m31);

        detsDeAdjunta.m33 = m.m00 * m.m11 * m.m22 + m.m01 * m.m12 * m.m20 + m.m02 * m.m10 * m.m21
                          - m.m02 * m.m11 * m.m20 - m.m01 * m.m10 * m.m22 - m.m00 * m.m12 * m.m21;

        // calculamos la traspuesta de la matriz adjunta
        MyMatrix4x4 transpoceDetsAdjunta = Transpose(detsDeAdjunta);

        //dividimos los componentes de la traspuesta de la matriz adjunta por el determinante de la matriz original
        detsDeAdjunta.m00 /= det; 
        detsDeAdjunta.m10 /=det; 
        detsDeAdjunta.m20 /=det; 
        detsDeAdjunta.m30 /=det; 
        detsDeAdjunta.m01 /=det; 
        detsDeAdjunta.m11 /=det; 
        detsDeAdjunta.m21 /=det; 
        detsDeAdjunta.m31 /=det; 
        detsDeAdjunta.m02 /=det; 
        detsDeAdjunta.m12 /=det; 
        detsDeAdjunta.m22 /=det; 
        detsDeAdjunta.m32 /=det; 
        detsDeAdjunta.m03 /=det; 
        detsDeAdjunta.m13 /=det; 
        detsDeAdjunta.m23 /=det;
        detsDeAdjunta.m33 /=det;

       // La matriz inversa es igual al inverso del valor de su determinante por la matriz traspuesta de la adjunta.
        return transpoceDetsAdjunta;

        //https://www.superprof.es/apuntes/escolar/matematicas/algebralineal/determinantes/matriz-inversa.html#:~:text=Cálculo%20por%20determinantes-,Definición%20de%20la%20matriz%20inversa,método%20por%20cálculo%20de%20determinantes.
    }

    public static MyMatrix4x4 Rotate(Quaternion q)
    {
        // Precalculate coordinate products
        float x = q.x * 2.0F;
        float y = q.y * 2.0F;
        float z = q.z * 2.0F;
        float xx = q.x * x;
        float yy = q.y * y;
        float zz = q.z * z;
        float xy = q.x * y;
        float xz = q.x * z;
        float yz = q.y * z;
        float wx = q.w * x;
        float wy = q.w * y;
        float wz = q.w * z;

        // Calculate 3x3 matrix from orthonormal basis
        MyMatrix4x4 m = identity;
        m.m00 = 1.0f - (yy + zz);   m.m01 = xy - wz;            m.m02 = xz + wy;            m.m03 = 0.0F;
        m.m10 = xy + wz;            m.m11 = 1.0f - (xx + zz);   m.m12 = yz - wx;            m.m13 = 0.0F;
        m.m20 = xz - wy;            m.m21 = yz + wx;            m.m22 = 1.0f - (xx + yy);   m.m23 = 0.0F;
        m.m30 = 0.0F;               m.m31 = 0.0F;               m.m32 = 0.0F;               m.m33 = 1.0F;

        return m;
    }

    public static MyMatrix4x4 Scale(Vector3 vector)
    {
        MyMatrix4x4 m = identity;
        m.m00 = vector.x; m.m01 = 0F; m.m02 = 0F; m.m03 = 0F;
        m.m10 = 0F; m.m11 = vector.y; m.m12 = 0F; m.m13 = 0F;
        m.m20 = 0F; m.m21 = 0F; m.m22 = vector.z; m.m23 = 0F;
        m.m30 = 0F; m.m31 = 0F; m.m32 = 0F; m.m33 = 1F;
        return m;
    }

    public static MyMatrix4x4 Translate(Vector3 vector)
    {
        MyMatrix4x4 m = identity;
        m.m00 = 1F; m.m01 = 0F; m.m02 = 0F; m.m03 = vector.x;
        m.m10 = 0F; m.m11 = 1F; m.m12 = 0F; m.m13 = vector.y;
        m.m20 = 0F; m.m21 = 0F; m.m22 = 1F; m.m23 = vector.z;
        m.m30 = 0F; m.m31 = 0F; m.m32 = 0F; m.m33 = 1F;
        return m;
    }

    public static MyMatrix4x4 Transpose(MyMatrix4x4 m)
    {
        MyMatrix4x4 result = identity;

        result.m00 = m.m00;
        result.m01 = m.m10;
        result.m02 = m.m20;
        result.m03 = m.m30;
        result.m10 = m.m01;
        result.m11 = m.m11;
        result.m12 = m.m21;
        result.m13 = m.m31;
        result.m20 = m.m02;
        result.m21 = m.m12;
        result.m22 = m.m22;
        result.m23 = m.m32;
        result.m30 = m.m03;
        result.m31 = m.m13;
        result.m32 = m.m23;
        result.m33 = m.m33;

        return result;
    }

    public static MyMatrix4x4 TRS(Vector3 pos, Quaternion q, Vector3 s)
    {
        return Translate(pos) * Rotate(q) * Scale(s);
    }

    public override bool Equals(object other)
    {
        if (!(other is MyMatrix4x4)) return false;

        return Equals((MyMatrix4x4)other);
    }

    public bool Equals(MyMatrix4x4 other)
    {
        return GetColumn(0).Equals(other.GetColumn(0))
                && GetColumn(1).Equals(other.GetColumn(1))
                && GetColumn(2).Equals(other.GetColumn(2))
                && GetColumn(3).Equals(other.GetColumn(3));
    }

    public Vector4 GetColumn(int index)
    {
        switch (index)
        {
            case 0: return new Vector4(m00, m10, m20, m30);
            case 1: return new Vector4(m01, m11, m21, m31);
            case 2: return new Vector4(m02, m12, m22, m32);
            case 3: return new Vector4(m03, m13, m23, m33);
            default:
                throw new IndexOutOfRangeException("Invalid column index!");
        }
    }

    public Vector4 GetRow(int index)
    {
        switch (index)
        {
            case 0: return new Vector4(m00, m01, m02, m03);
            case 1: return new Vector4(m10, m11, m12, m13);
            case 2: return new Vector4(m20, m21, m22, m23);
            case 3: return new Vector4(m30, m31, m32, m33);
            default:
                throw new IndexOutOfRangeException("Invalid row index!");
        }
    }

    public Vector3 MultiplyPoint(Vector3 point)
    {
        Vector3 res;
        float w;
        res.x = m00 * point.x + m01 * point.y + m02 * point.z + m03;
        res.y = m10 * point.x + m11 * point.y + m12 * point.z + m13;
        res.z = m20 * point.x + m21 * point.y + m22 * point.z + m23;
        w =     m30 * point.x + m31 * point.y + m32 * point.z + m33;

        w = 1F / w;
        res.x *= w;
        res.y *= w;
        res.z *= w;
        return res;
    }

    public Vector3 MultiplyPoint3x4(Vector3 point)
    {
        Vector3 res;
        res.x = m00 * point.x + m01 * point.y + m02 * point.z + m03;
        res.y = m10 * point.x + m11 * point.y + m12 * point.z + m13;
        res.z = m20 * point.x + m21 * point.y + m22 * point.z + m23;
        return res;
    }

    public Vector3 MultiplyVector(Vector3 vector)
    {
        Vector3 res;
        res.x = m00 * vector.x + m01 * vector.y + m02 * vector.z;
        res.y = m10 * vector.x + m11 * vector.y + m12 * vector.z;
        res.z = m20 * vector.x + m21 * vector.y + m22 * vector.z;
        return res;
    }

    public void SetColumn(int index, Vector4 column)
    {
        this[0, index] = column.x;
        this[1, index] = column.y;
        this[2, index] = column.z;
        this[3, index] = column.w;
    }

    public void SetRow(int index, Vector4 row)
    {
        this[index, 0] = row.x;
        this[index, 1] = row.y;
        this[index, 2] = row.z;
        this[index, 3] = row.w;
    }

    public void SetTRS(Vector3 pos, Quaternion q, Vector3 s)
    {
        this = TRS(pos, q, s);
    }

    public static Vector4 operator *(MyMatrix4x4 lhs, Vector4 vector)
    {
        Vector4 res;
        res.x = lhs.m00 * vector.x + lhs.m01 * vector.y + lhs.m02 * vector.z + lhs.m03 * vector.w;
        res.y = lhs.m10 * vector.x + lhs.m11 * vector.y + lhs.m12 * vector.z + lhs.m13 * vector.w;
        res.z = lhs.m20 * vector.x + lhs.m21 * vector.y + lhs.m22 * vector.z + lhs.m23 * vector.w;
        res.w = lhs.m30 * vector.x + lhs.m31 * vector.y + lhs.m32 * vector.z + lhs.m33 * vector.w;
        return res;
    }

    public static MyMatrix4x4 operator *(MyMatrix4x4 lhs, MyMatrix4x4 rhs)
    {
        MyMatrix4x4 res = identity;

        res.m00 = lhs.m00 * rhs.m00 + lhs.m01 * rhs.m10 + lhs.m02 * rhs.m20 + lhs.m03 * rhs.m30;
        res.m01 = lhs.m00 * rhs.m01 + lhs.m01 * rhs.m11 + lhs.m02 * rhs.m21 + lhs.m03 * rhs.m31;
        res.m02 = lhs.m00 * rhs.m02 + lhs.m01 * rhs.m12 + lhs.m02 * rhs.m22 + lhs.m03 * rhs.m32;
        res.m03 = lhs.m00 * rhs.m03 + lhs.m01 * rhs.m13 + lhs.m02 * rhs.m23 + lhs.m03 * rhs.m33;

        res.m10 = lhs.m10 * rhs.m00 + lhs.m11 * rhs.m10 + lhs.m12 * rhs.m20 + lhs.m13 * rhs.m30;
        res.m11 = lhs.m10 * rhs.m01 + lhs.m11 * rhs.m11 + lhs.m12 * rhs.m21 + lhs.m13 * rhs.m31;
        res.m12 = lhs.m10 * rhs.m02 + lhs.m11 * rhs.m12 + lhs.m12 * rhs.m22 + lhs.m13 * rhs.m32;
        res.m13 = lhs.m10 * rhs.m03 + lhs.m11 * rhs.m13 + lhs.m12 * rhs.m23 + lhs.m13 * rhs.m33;

        res.m20 = lhs.m20 * rhs.m00 + lhs.m21 * rhs.m10 + lhs.m22 * rhs.m20 + lhs.m23 * rhs.m30;
        res.m21 = lhs.m20 * rhs.m01 + lhs.m21 * rhs.m11 + lhs.m22 * rhs.m21 + lhs.m23 * rhs.m31;
        res.m22 = lhs.m20 * rhs.m02 + lhs.m21 * rhs.m12 + lhs.m22 * rhs.m22 + lhs.m23 * rhs.m32;
        res.m23 = lhs.m20 * rhs.m03 + lhs.m21 * rhs.m13 + lhs.m22 * rhs.m23 + lhs.m23 * rhs.m33;

        res.m30 = lhs.m30 * rhs.m00 + lhs.m31 * rhs.m10 + lhs.m32 * rhs.m20 + lhs.m33 * rhs.m30;
        res.m31 = lhs.m30 * rhs.m01 + lhs.m31 * rhs.m11 + lhs.m32 * rhs.m21 + lhs.m33 * rhs.m31;
        res.m32 = lhs.m30 * rhs.m02 + lhs.m31 * rhs.m12 + lhs.m32 * rhs.m22 + lhs.m33 * rhs.m32;
        res.m33 = lhs.m30 * rhs.m03 + lhs.m31 * rhs.m13 + lhs.m32 * rhs.m23 + lhs.m33 * rhs.m33;

        return res;
    }

    public static bool operator ==(MyMatrix4x4 lhs, MyMatrix4x4 rhs)
    {
        return lhs.GetColumn(0) == rhs.GetColumn(0)
                && lhs.GetColumn(1) == rhs.GetColumn(1)
                && lhs.GetColumn(2) == rhs.GetColumn(2)
                && lhs.GetColumn(3) == rhs.GetColumn(3);
    }

    public static bool operator !=(MyMatrix4x4 lhs, MyMatrix4x4 rhs)
    {
        return !(lhs == rhs);
    }
}
