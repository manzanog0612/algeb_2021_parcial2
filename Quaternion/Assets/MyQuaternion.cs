﻿#region ensamblado UnityEngine.CoreModule, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// C:\Program Files\Unity\Hub\Editor\2019.4.21f1\Editor\Data\Managed\UnityEngine\UnityEngine.CoreModule.dll
#endregion

using System;
using UnityEngine;
using System.Reflection;
using UnityEngine.Bindings;
using UnityEngine.Internal;
using UnityEngine.Scripting;

public struct MyQuaternion : IEquatable<MyQuaternion>
{
    public const float kEpsilon = 1E-06F;
    public float x;
    public float y; 
    public float z;
    public float w;

    static MyQuaternion FromEulerToQuaternion(Vector3 euler)
    {
        float sinAngle = 0.0f;
        float cosAngle = 0.0f;

        MyQuaternion qx = identity;
        MyQuaternion qy = identity;
        MyQuaternion qz = identity;
        MyQuaternion r = identity;

        sinAngle = Mathf.Sin(Mathf.Deg2Rad * euler.z * 0.5f);
        cosAngle = Mathf.Cos(Mathf.Deg2Rad * euler.z * 0.5f);
        qz.Set(0, 0, sinAngle, cosAngle);

        sinAngle = Mathf.Sin(Mathf.Deg2Rad * euler.x * 0.5f);
        cosAngle = Mathf.Cos(Mathf.Deg2Rad * euler.x * 0.5f);
        qx.Set(sinAngle, 0, 0, cosAngle);

        sinAngle = Mathf.Sin(Mathf.Deg2Rad * euler.y * 0.5f);
        cosAngle = Mathf.Cos(Mathf.Deg2Rad * euler.y * 0.5f);
        qy.Set(0, sinAngle, 0, cosAngle);

        r = qy * qx * qz;

        return r;
    }

    public static Vector3 FromQuaternionToEuler(MyQuaternion rotation)
    {
        float sqw = rotation.w * rotation.w;
        float sqx = rotation.x * rotation.x;
        float sqy = rotation.y * rotation.y;
        float sqz = rotation.z * rotation.z;
        float unit = sqx + sqy + sqz + sqw; // if normalised is one, otherwise is correction factor
        float test = rotation.x * rotation.w - rotation.y * rotation.z;
        Vector3 v;

        if (test > 0.4999f * unit)   // singularity at north pole
        { 
            v.y = 2f * Mathf.Atan2(rotation.y, rotation.x);
            v.x = Mathf.PI / 2;
            v.z = 0;
            return NormalizeAngles(v * Mathf.Rad2Deg);
        }
        if (test < -0.4999f * unit)  // singularity at south pole
        { 
            v.y = -2f * Mathf.Atan2(rotation.y, rotation.x);
            v.x = -Mathf.PI / 2;
            v.z = 0;
            return NormalizeAngles(v * Mathf.Rad2Deg);
        }

        MyQuaternion q = new MyQuaternion(rotation.w, rotation.z, rotation.x, rotation.y);
        v.y = Mathf.Atan2(2f * q.x * q.w + 2f * q.y * q.z, 1 - 2f * (q.z * q.z + q.w * q.w));     // Yaw
        v.x = Mathf.Asin(2f * (q.x * q.z - q.w * q.y));                                           // Pitch
        v.z = Mathf.Atan2(2f * q.x * q.y + 2f * q.z * q.w, 1 - 2f * (q.y * q.y + q.z * q.z));      // Roll
        return NormalizeAngles(v * Mathf.Rad2Deg);
    }

    private static Vector3 NormalizeAngles(Vector3 angles)
    {
        angles.x = NormalizeAngle(angles.x);
        angles.y = NormalizeAngle(angles.y);
        angles.z = NormalizeAngle(angles.z);
        return angles;
    }

    private static float NormalizeAngle(float angle)
    {
        while (angle > 360)
            angle -= 360;
        while (angle < 0)
            angle += 360;
        return angle;
    }

    public MyQuaternion(float x, float y, float z, float w)
    {
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
    }

    public MyQuaternion(Vector3 v, float w)
    {
        x = v.x;
        y = v.y;
        z = v.z;
        this.w = w;
    }

    public MyQuaternion(Quaternion q)
    {
        this.x = q.x;
        this.y = q.y;
        this.z = q.z;
        this.w = q.w;
    }

    public float this[int index]
    {
        get
        {
            switch (index)
            {
                case 0: return x;
                case 1: return y;
                case 2: return z;
                case 3: return w;
                default: return x;
            }
        }
        set
        {
            switch (index)
            {
                case 0: x = value; break;
                case 1: y = value; break;
                case 2: z = value; break;
                case 3: w = value; break;
                default: break;
            }
        }
    }

    static readonly MyQuaternion identityQuaternion = new MyQuaternion(0F, 0F, 0F, 1F);

    public static MyQuaternion identity
    {
        get
        {
            return identityQuaternion;
        }
    }

    public float Length
    {
        get
        {
            return Mathf.Sqrt(x * x + y * y + z * z + w * w);
        }
    }

    public float LengthSquared
    {
        get
        {
            return x * x + y * y + z * z + w * w;
        }
    }

    public Vector3 xyz
    {
        set
        {
            x = value.x;
            y = value.y;
            z = value.z;
        }
        get
        {
            return new Vector3(x, y, z);
        }
    }

    public Vector3 eulerAngles
    {
        get 
        {
            return FromQuaternionToEuler(this);
        }
        set 
        {
            this = FromEulerToQuaternion(value);
        }
    }

    public MyQuaternion normalized
    {
        get { return Normalize(this); }
    }

    public static float Angle(MyQuaternion a, MyQuaternion b)
    {
        float dot = Dot(a, b);
        float dotAbs = Mathf.Abs(dot);
        return IsEqualUsingDot(Dot(a, b)) ? 0.0f : Mathf.Acos(Mathf.Min(dotAbs, 1.0F)) * 2.0f * Mathf.Rad2Deg;
    }

    public static MyQuaternion AngleAxis(float angle, Vector3 axis)
    {
        axis.Normalize();
        axis *= Mathf.Sin(angle * Mathf.Deg2Rad * 0.5f);
        return new MyQuaternion(axis.x, axis.y, axis.z, Mathf.Cos(angle * Mathf.Deg2Rad * 0.5f));
    }

    public static MyQuaternion AxisAngle(Vector3 axis, float angle)
    { 
        return AngleAxis(Mathf.Rad2Deg * angle, axis); 
    }

    public static float Dot(MyQuaternion a, MyQuaternion b)
    {
        return a.x * b.x + a.y * b.y + a.z * b.z + a.w * b.w;
    }

    public static MyQuaternion Euler(Vector3 euler)
    { 
        return FromEulerToQuaternion(euler); 
    }

    public static MyQuaternion Euler(float x, float y, float z) 
    { 
        return FromEulerToQuaternion(new Vector3(x,y,z)); 
    }

    static public MyQuaternion EulerAngles(float x, float y, float z) 
    {
        return FromEulerToQuaternion(new Vector3(x, y, z));
    }

    public static MyQuaternion EulerAngles(Vector3 euler) 
    {
        return FromEulerToQuaternion(euler);
    }

    static public MyQuaternion EulerRotation(float x, float y, float z)
    { 
        return FromEulerToQuaternion(new Vector3(x, y, z));
    }

    public static MyQuaternion EulerRotation(Vector3 euler)
    { 
        return FromEulerToQuaternion(euler);
    }

    public static MyQuaternion FromToRotation(Vector3 fromDirection, Vector3 toDirection)
    {
        Vector3 axis = Vector3.Cross(fromDirection, toDirection);
        float angle = Vector3.Angle(fromDirection, toDirection);
        return AngleAxis(angle, axis.normalized);
    }

    public static MyQuaternion Inverse(MyQuaternion rotation)
    {
        return new MyQuaternion(-rotation.x, -rotation.y, -rotation.z, rotation.w);
    }

    public static MyQuaternion Lerp(MyQuaternion a, MyQuaternion b, float t)
    {
        if (t < 0f) t = 0f;
        if (t > 1f) t = 1f;

        return LerpUnclamped(a, b, t);
    }

    public static MyQuaternion LerpUnclamped(MyQuaternion a, MyQuaternion b, float t)
    {
        MyQuaternion result = identity;

        float timeLeft = 1f - t;

        if (Dot(a, b) >= 0f)
        {
            result.x = (timeLeft * a.x) + (t * b.x);
            result.y = (timeLeft * a.y) + (t * b.y);
            result.z = (timeLeft * a.z) + (t * b.z);
            result.w = (timeLeft * a.w) + (t * b.w);
        }
        else
        {
            result.x = (timeLeft * a.x) - (t * b.x);
            result.y = (timeLeft * a.y) - (t * b.y);
            result.z = (timeLeft * a.z) - (t * b.z);
            result.w = (timeLeft * a.w) - (t * b.w);
        }

        result.Normalize();

        return result;
    }

    public static MyQuaternion LookRotation(Vector3 forward)
    {
        return LookRotation(forward, Vector3.up);
    }

    private static MyQuaternion LookRotation(Vector3 forward, Vector3 up)
    {
        forward = Vector3.Normalize(forward);
        Vector3 right = Vector3.Normalize(Vector3.Cross(up, forward));
        up = Vector3.Cross(forward, right);

        float m00 = right.x;    float m01 = right.y;    float m02 = right.z;
        float m10 = up.x;       float m11 = up.y;       float m12 = up.z;
        float m20 = forward.x;  float m21 = forward.y;  float m22 = forward.z;

        float diagonals = m00 + m11 + m22;
        var quaternion = new MyQuaternion();
        if (diagonals > 0f)
        {
            float num = Mathf.Sqrt(diagonals + 1f);
            quaternion.w = num * 0.5f;
            num = 0.5f / num;
            quaternion.x = (m12 - m21) * num;
            quaternion.y = (m20 - m02) * num;
            quaternion.z = (m01 - m10) * num;
            return quaternion;
        }
        if (m00 >= m11 && m00 >= m22)
        {
            float num = Mathf.Sqrt(1f + m00 - m11 - m22);
            float num4 = 0.5f / num;
            quaternion.x = 0.5f * num;
            quaternion.y = (m01 + m10) * num4;
            quaternion.z = (m02 + m20) * num4;
            quaternion.w = (m12 - m21) * num4;
            return quaternion;
        }
        if (m11 > m22)
        {
            float num = Mathf.Sqrt(1f + m11 - m00 - m22);
            float num3 = 0.5f / num;
            quaternion.x = (m10 + m01) * num3;
            quaternion.y = 0.5f * num;
            quaternion.z = (m21 + m12) * num3;
            quaternion.w = (m20 - m02) * num3;
            return quaternion;
        }

        float num5 = Mathf.Sqrt(1f + m22 - m00 - m11);
        float num2 = 0.5f / num5;
        quaternion.x = (m20 + m02) * num2;
        quaternion.y = (m21 + m12) * num2;
        quaternion.z = 0.5f * num5;
        quaternion.w = (m01 - m10) * num2;

        return quaternion;
    }

    public void SetLookRotation(Vector3 view)
    {
        Vector3 up = Vector3.up;
        SetLookRotation(view, up);
    }

    public static MyQuaternion Normalize(MyQuaternion q)
    {
        float mag = Mathf.Sqrt(Dot(q, q));

        if (mag < Mathf.Epsilon)
            return identity;

        return new MyQuaternion(q.x / mag, q.y / mag, q.z / mag, q.w / mag);
    }

    public static MyQuaternion RotateTowards(MyQuaternion from, MyQuaternion to, float maxDegreesDelta)
    {
        float angle = Angle(from, to);
        if (angle == 0.0f) return to;
        return SlerpUnclamped(from, to, Mathf.Min(1.0f, maxDegreesDelta / angle));
    }

    public static MyQuaternion Slerp(MyQuaternion a, MyQuaternion b, float t)
    {
        if (t < 0f) t = 0f;
        if (t > 1f) t = 1f;

        return SlerpUnclamped(a, b, t);//esto no es
    }

    public static MyQuaternion SlerpUnclamped(MyQuaternion a, MyQuaternion b, float t)
    {
        if (a.LengthSquared == 0.0f)
        {
            if (b.LengthSquared == 0.0f)
            {
                return identity;
            }
            return b;
        }
        else if (b.LengthSquared == 0.0f)
        {
            return a;
        }

        float dot = Dot(a,b);

        if (Dot(a, b) >= 1.0f || Dot(a, b) <= -1.0f)
        {
            // angle = 0.0f, so just return one input.
            return a;
        }
        else if (dot < 0.0f)
        {
            b.xyz = -b.xyz;
            b.w = -b.w;
            dot = -dot;
        }

        float blendA;
        float blendB;
        if (dot < 0.99f)
        {
            // do proper slerp for big angles
            float halfAngle = Mathf.Acos(dot);
            float sinHalfAngle = Mathf.Sin(halfAngle);
            float oneOverSinHalfAngle = 1.0f / sinHalfAngle;
            blendA = Mathf.Sin(halfAngle * (1.0f - t)) * oneOverSinHalfAngle;
            blendB = Mathf.Sin(halfAngle * t) * oneOverSinHalfAngle;
        }
        else
        {
            // do lerp if angle is really small.
            blendA = 1.0f - t;
            blendB = t;
        }

        MyQuaternion result = new MyQuaternion(blendA * a.xyz + blendB * b.xyz, blendA * a.w + blendB * b.w);
        if (result.LengthSquared > 0.0f)
            return Normalize(result);
        else
            return identity;
    }

    #region done
    public Vector3 ToEulerAngles() 
    { 
        return FromQuaternionToEuler(this); 
    }

    public bool Equals(MyQuaternion other)
    {
        return x.Equals(other.x) && y.Equals(other.y) && z.Equals(other.z) && w.Equals(other.w);
    }

    public override bool Equals(object other)
    {
        if (!(other is MyQuaternion)) return false;

        return Equals((MyQuaternion)other);
    }

    public override int GetHashCode()
    {
        return x.GetHashCode() ^ (y.GetHashCode() << 2) ^ (z.GetHashCode() >> 2) ^ (w.GetHashCode() >> 1);
    }

    public void Normalize()
    {
        this = Normalize(this);
    }

    public void Set(float newX, float newY, float newZ, float newW)
    {
        x = newX;
        y = newY;
        z = newZ;
        w = newW;
    }

    public void SetAxisAngle(Vector3 axis, float angle) 
    {
        this = AxisAngle(axis, angle); 
    }

    public void SetEulerAngles(Vector3 euler)
    {
        this = FromEulerToQuaternion(euler);
    }

    public void SetEulerAngles(float x, float y, float z)
    {
        this = FromEulerToQuaternion(new Vector3(x,y,z));
    }

    public void SetEulerRotation(float x, float y, float z)
    {
        SetEulerAngles(x, y, z);
    }

    public void SetEulerRotation(Vector3 euler)
    {
        SetEulerAngles(euler);
    }

    public void SetFromToRotation(Vector3 fromDirection, Vector3 toDirection) 
    { 
        this = FromToRotation(fromDirection, toDirection); 
    }

    public void SetLookRotation(Vector3 view, Vector3 up)
    {
        this = LookRotation(view, up);
    }

    public void ToAngleAxis(out float angle, out Vector3 axis)
    {
        ToAxisAngle(out axis, out angle);
        angle *= Mathf.Rad2Deg;
    }

    public void ToAxisAngle(out Vector3 axis, out float angle)
    {
        Normalize();
        angle = 2.0f * Mathf.Acos(w);
        float mag = Mathf.Sqrt(1.0f - w * w);
        if (mag > 0.0001f)
        {
            axis = new Vector3(x,y,z) / mag;
        }
        else
        {
            // si el angulo es 0 se pasa un eje arbitrario
            axis = new Vector3(1, 0, 0);
        }
    }

    public Vector3 ToEuler() 
    {
        return FromQuaternionToEuler(this);
    }

    public override string ToString()
    {
        return string.Format("({0:F1}, {1:F1}, {2:F1}, {3:F1})", x, y, z, w);
    }

    public string ToString(string format)
    {
        return string.Format("({0}, {1}, {2}, {3})", x.ToString(format), y.ToString(format), z.ToString(format), w.ToString(format));
    }

    public static Vector3 operator *(MyQuaternion rotation, Vector3 point)
    {
        MyQuaternion p = new MyQuaternion(point.x, point.y, point.z, 0);
        MyQuaternion p2 = (rotation * p) * Inverse(rotation);
        Vector3 res = new Vector3(p2.x, p2.y, p2.z);
        return res;
    }

    public static MyQuaternion operator *(MyQuaternion lhs, MyQuaternion rhs)
    {
        return new MyQuaternion(
            lhs.w * rhs.x + lhs.x * rhs.w + lhs.y * rhs.z - lhs.z * rhs.y,
            lhs.w * rhs.y + lhs.y * rhs.w + lhs.z * rhs.x - lhs.x * rhs.z,
            lhs.w * rhs.z + lhs.z * rhs.w + lhs.x * rhs.y - lhs.y * rhs.x,
            lhs.w * rhs.w - lhs.x * rhs.x - lhs.y * rhs.y - lhs.z * rhs.z);
    }

    public static bool operator ==(MyQuaternion lhs, MyQuaternion rhs)
    {
        return IsEqualUsingDot(Dot(lhs, rhs));
    }

    public static bool operator !=(MyQuaternion lhs, MyQuaternion rhs)
    {
        // Returns true in the presence of NaN values.
        return !(lhs == rhs);
    }

    private static bool IsEqualUsingDot(float dot)
    {
        // Returns false in the presence of NaN values.
        return dot > 1.0f - kEpsilon;
    }

    #endregion
}
