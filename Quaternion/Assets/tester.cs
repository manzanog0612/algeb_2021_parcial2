﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tester : MonoBehaviour
{
    [SerializeField] Transform son;
    public Vector3 angle;
    public float myAngle;
    public float Angle;
    public Vector3 euler = Vector3.zero;
    public Vector3 point = Vector3.zero;
    public Vector3 rotatedPoint = Vector3.zero;
    public Vector3 myRotatedPoint = Vector3.zero;

    float sinAngle = 0.0f;
    float cosAngle = 0.0f;

    public MyQuaternion qX = MyQuaternion.identity;
    public Quaternion qx = Quaternion.identity;
    public Quaternion qy = Quaternion.identity;
    Quaternion qz = Quaternion.identity;

    public Quaternion rotationBA = Quaternion.identity;
    public Quaternion myRotationBA = Quaternion.identity;
    public Quaternion angleBQ = Quaternion.identity;
    public Quaternion myAngleBQ = Quaternion.identity;
    public Vector3 v1 = Vector3.zero;
    public Vector3 v2 = Vector3.zero;
    public Vector3 v3 = Vector3.zero;
    public Vector3 v4 = Vector3.zero;
    public Vector3 axis;
    public Vector3 myaxis;

    public Quaternion r = Quaternion.identity;
    public MyQuaternion rotation = MyQuaternion.identity;

    public float t = 0f;

    public enum funciones
    {
        angle, toAxisAngle, toAngleAxis, euler, lerp, AxisAngle, rotatingAPoint, lookRotation
    }
    public funciones funcion = funciones.angle;

    void Update()
    {
        qx = Quaternion.Euler(v3);
        qX = MyQuaternion.Euler(v3);
        qy = Quaternion.Euler(v4);

        switch (funcion)
        {
            case funciones.angle:
                { 
                    //angulos - modificar v3 y v4 para ver
                    myAngle = MyQuaternion.Angle(new MyQuaternion(qx), new MyQuaternion(qy));
                    Angle = Quaternion.Angle(qx, qy);
                }
                break;
            case funciones.toAxisAngle:
                {
                    //toAxisAngle  - modificar v3 para ver
                    qx.ToAxisAngle(out v1, out Angle);
                    qX.ToAxisAngle(out v2, out myAngle);
                }
                break;
            case funciones.toAngleAxis:
                {
                    //toAngleAxis  - modificar v3 para ver
                    qx.ToAngleAxis(out Angle, out v1);
                    qX.ToAngleAxis(out myAngle, out v2);
                }
                break;
            case funciones.euler:
                {
                    //euler - modificar v3 para ver
                    v1 = qx.eulerAngles;
                    rotation = new MyQuaternion(qx);
                    v2 = rotation.eulerAngles;

                    transform.rotation = Quaternion.Euler(v1);
                    son.rotation = Quaternion.Euler(v2);
                }
                break;
            case funciones.lerp:
                {
                    //lerp - modificar v3 y v4 para ver - se ve en scene no en game
                    r = Quaternion.Lerp(qx, qy, t);
                    v1 = r.eulerAngles;
                    rotation = MyQuaternion.Lerp(new MyQuaternion(qx), new MyQuaternion(qy), t);
                    myRotationBA = new Quaternion(rotation.x, rotation.y, rotation.z, rotation.w);
                    v2 = rotation.eulerAngles;
                    t += Time.deltaTime;

                    if (t > 1)
                        t = 0;

                    Debug.DrawLine(Vector3.zero, r.eulerAngles, Color.red);
                    Debug.DrawLine(Vector3.zero, rotation.eulerAngles, Color.blue);
                }
                break;
            case funciones.AxisAngle:
                {
                    //angulos axis - modificar angle.x vec3 para ver
                    angleBQ = Quaternion.AxisAngle(Vector3.right, angle.x);
                    myAngleBQ = new Quaternion(MyQuaternion.AxisAngle(Vector3.right, angle.x).x, MyQuaternion.AxisAngle(Vector3.right, angle.x).y,
                                              MyQuaternion.AxisAngle(Vector3.right, angle.x).z, MyQuaternion.AxisAngle(Vector3.right, angle.x).w);
                }
                break;
            case funciones.rotatingAPoint:
                {
                    //rotating a point - modificar v3 para ver
                    r = Quaternion.Euler(v3);
                    rotation = new MyQuaternion(r.x, r.y, r.z, r.w);

                    rotatedPoint = r * point;
                    myRotatedPoint = rotation * point;

                    Debug.DrawLine(Vector3.zero, point, Color.red);
                    Debug.DrawLine(Vector3.zero, rotatedPoint, Color.blue);
                    Debug.DrawLine(Vector3.zero, myRotatedPoint, Color.green);
                }
                break;
            case funciones.lookRotation:
                {
                    //look rotationt - modificar v3 para ver
                    transform.rotation = Quaternion.LookRotation(v3);
                    MyQuaternion q = MyQuaternion.LookRotation(v3);
                    Quaternion q1 = new Quaternion(q.x, q.y, q.z, q.w);

                    son.transform.rotation = q1;
                }
                break;
            default:
                break;
        }

        /*rotationBA = Quaternion.FromToRotation(v1, v2);
         rotationBA.ToAxisAngle(out axis, out Angle);

         MyQuaternion mrBA = MyQuaternion.FromToRotation(v1, v2);
         mrBA.ToAxisAngle(out myaxis, out myAngle);
         myRotationBA = new Quaternion(mrBA.x, mrBA.y, mrBA.z, mrBA.w);*/
    }
}
