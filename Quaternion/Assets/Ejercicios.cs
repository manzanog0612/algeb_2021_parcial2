﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ejercicios : MonoBehaviour
{
    public enum Ejercicio
    {
        uno, dos, tres
    }

    public Ejercicio ej = Ejercicio.uno;
    Ejercicio anteriorEj = Ejercicio.uno;
    public float angle = 0;
    float distance = 10f;

    public Vector3 point1;
    public Vector3 point2;
    public Vector3 point3;
    public Vector3 point4;

    void Start()
    {
        point1 = new Vector3(distance, 0f, 0f);
    }

    void FixedUpdate()
    {
        MyQuaternion rot;
       
        switch (ej)
        {
            case Ejercicio.uno:
                ej = Ejercicio.uno;

                if (anteriorEj == Ejercicio.tres)
                {
                    point1 = new Vector3(distance, 0f, 0f);
                }

                rot = MyQuaternion.EulerAngles(0f, angle * Mathf.Rad2Deg * Time.fixedDeltaTime, 0f);
                
                point1 = rot * point1;

                Debug.DrawLine(transform.position, point1, Color.red);
                break;
            case Ejercicio.dos:
                ej = Ejercicio.dos;

                if (anteriorEj == Ejercicio.tres)
                {
                    point1 = new Vector3(distance, 0f, 0f);
                }

                rot = MyQuaternion.EulerAngles(0f, angle * Mathf.Rad2Deg * Time.fixedDeltaTime, 0f);
                
                point1 = rot * point1;
                point2 = point1 + new Vector3(0f, distance, 0f);
                point3 = point1 * 2 + new Vector3(0f, distance, 0f);

                Debug.DrawLine(transform.position, point1, Color.red);
                Debug.DrawLine(point1, point2, Color.red);
                Debug.DrawLine(point2, point3, Color.red);

                break;
            case Ejercicio.tres:
                ej = Ejercicio.tres;

                if (anteriorEj!=ej)
                {
                    point1 = new Vector3(distance, 0f, 0f);
                    point3 = new Vector3(distance * 2f, distance, 0f);
                }
                
                rot = MyQuaternion.EulerAngles(angle * Mathf.Rad2Deg * Time.fixedDeltaTime, angle * Mathf.Rad2Deg * Time.fixedDeltaTime, 0f);

                point1 = rot * point1;
                point2 = new Vector3(distance, distance, 0f);
                point3 = MyQuaternion.Inverse(rot) * point3;
                point4 = new Vector3(distance * 2, distance * 2, 0f);

                Debug.DrawLine(transform.position, point1, Color.red);
                Debug.DrawLine(point1, point2, Color.red);
                Debug.DrawLine(point2, point3, Color.red);
                Debug.DrawLine(point3, point4, Color.red);
                break;
            default:
                break;
        }

        anteriorEj = ej;
    }
}
